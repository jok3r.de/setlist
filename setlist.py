#!/usr/bin/env python3

import codecs
import sys
import getopt

htmlpage = u"""<html>
<head>
  <title>{0}</title>
  <meta charset="utf-8"/>
</head>
<body style="width: {3}cm">
<p style="text-align: center">{2}</p>
<p style="text-align: center">{0}</p>
{1}
</body>
</html>"""


def read_file(filename):
    with codecs.open(filename, "r", "utf-8") as f:
        lines = f.readlines()
        lines = [line.strip() for line in lines]
        return lines


def write_to_file(data, filename):
    with codecs.open(filename, "w", "utf-8") as f:
        f.write(data)


def split_sets(lines):
    sets = []
    songs = []
    for line in lines:
        if line != '':
            songs.append(line)
        else:
            sets.append(songs)
            songs = []
            continue
    if len(songs) > 0:
        sets.append(songs)
    return sets


def make_table_for_set(lines, setnumber, width):
    if lines[0] == "Reserve:":
        return "<p style=\"clear: both; \">Reserve: " + ", ".join(lines[1:]) + "</p>"
    for i in range(0, len(lines)):
        lines[i] = "\t<tr><td style=\"font-size:10pt\">" + str(i + 1) + "</td><td>" + lines[i] + "</td></tr>\n"
    begin = "<table style=\"font-size: 18pt; margin: 10 5 0 5; width: " + width + "\">\n"
    header = "\t<tr><th></th><th style=\"color: red; text-align: left; font-size: 12pt;\">Runde " + str(setnumber) + "</th></tr>\n"
    end = "</table>"
    lines = [begin] + [header] + lines + [end]
    table = "".join(lines)
    return table


def make_tables_for_sets(sets):
    # return "\n".join([make_table_for_set(lines) for lines in sets])
    i = 1
    res = ""
    mod = 2
    width = "50%"
    if len(sets) % 2 == 0:
        mod = 3
        width = "33%"
    for lines in sets:
        if i % mod == 1:
            res = res + "<div style=\"display: flex;\">"
        res = res + make_table_for_set(lines, i, width) + "\n"
        if i % mod == 0:
            res = res + '</div>\n'
        i = i + 1
    return res, mod


def read_arguments(args):
    found_i = False
    found_o = False
    found_t = False

    inputfile = ""
    outputfile = ""
    title = ""
    picture = "header.png"
    width = "7cm"
    height = "2cm"

    try:
        opts, args = getopt.getopt(args[1:], "i:o:t:p:w:h:")
    except getopt.GetoptError:
        print("setlist.py -i <inputfile> -o <outputfile> -t <title> [-p <picturefile> -w <width> -h <height>]")
        sys.exit(2)
    for opt, arg in opts:
        if opt == "-i":
            inputfile = arg
            found_i = True
        elif opt == "-o":
            outputfile = arg
            found_o = True
        elif opt == "-t":
            title = arg
            found_t = True
        elif opt == "-p":
            picture = arg
        elif opt == "-w":
            width = arg
        elif opt == "-h":
            height = arg
    if not found_i or not found_o or not found_t:
        print("setlist.py -i <inputfile> -o <outputfile> -t <title> [-p <picturefile>] -w <width> -h <height>]")
        sys.exit(2)
    return inputfile, outputfile, title, picture, width, height


def create_picture(picture, width, height):
    if picture != "":
        picture = "<img src=\"" + picture + "\" style=\"width: " + width + "; height: " + height + "\"/>"
    return picture


def main():
    inputfile, outputfile, title, picture, width, height = read_arguments(sys.argv)
    picture = create_picture(picture, width, height)
    lines = read_file(inputfile)
    sets = split_sets(lines)
    tables, mod = make_tables_for_sets(sets)
    if mod == 3:
        width = 29.7
    else:
        width = 21
    output = htmlpage.format(title, tables, picture, width)
    print(output)
    write_to_file(output, outputfile)


if __name__ == '__main__':
    main()
